const Sequelize = require('sequelize');
const express = require('express')
const app = express()
const path = require('path');
const bodyParser = require('body-parser')
const port = 1979

const sequelize = new Sequelize('mysql://metcon:Jebac!979sad@87.98.236.38:3306/workout_logger', {
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

app.listen(port, () => console.log("WorkoutLogger by MetconLovers started working..."))

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const User = sequelize.define('user', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    pass: {
        type: Sequelize.STRING,
        allowNull: false
    },
    space: {
        type: Sequelize.BIGINT
    },
    type: {
        type: Sequelize.STRING(100)
    }
});

const Space = sequelize.define('space', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Workout = sequelize.define('workout', {
    day: {
        type: Sequelize.DATE,
        allowNull: false
    },
    desc: {
        type: Sequelize.STRING(10000),
        allowNull: false
    },
    image: {
        type: Sequelize.STRING(3000)
    },
    video: {
        type: Sequelize.STRING(3000)
    },
    tags: {
        type: Sequelize.STRING(3000)
    },
    space: {
        type: Sequelize.BIGINT,
        allowNull: false
    }
});

const Score = sequelize.define('score', {
    desc: {
        type: Sequelize.STRING(10000),
        allowNull: false
    },
    workout: {
        type: Sequelize.BIGINT,
        allowNull: false
    }
})

// sequelize.sync({
//     force: true
// })

app.use(bodyParser.json())
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/images', express.static(__dirname + '/images'));
app.use('/favcons', express.static(__dirname + '/favcons'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.post('/createSpace', function (req, res) {
    Space.create({
            name: req.body.space,
        })
        .then(space => {
            u = req.body.user
            u.space = space.id
            User.create(u)
                .then(user => {
                    res.status(200);
                    res.send(space);
                })
                .catch(err => {
                    res.status(419);
                    res.send(err);
                })
        })
        .catch(err => {
            res.status(419);
            res.send(err);
        })
})

app.post('/checkSpace', function (req, res) {
    Space.findOne({
        where: {
            name: req.body.space
        }
    }).then(space => {
        if (space) {
            res.status(200);
            res.send(space);
        } else {
            res.status(404);
            res.send("Space not exist");
        }
    })
})

app.post('/workout', function (req, res) {
    if (req.body.id) {
        Workout.update(req.body, {
                where: {
                    id: req.body.id
                }
            })
            .then(() => {
                res.status(200);
                res.send("OK");
            })
            .catch(err => {
                res.status(419);
                res.send(err);
            });
    } else {
        Workout.create(req.body)
            .then(workout => {
                res.status(200);
                res.send(workout);
            })
            .catch(err => {
                res.status(419);
                res.send(err);
            })
    }
})

app.post('/workouts', function (req, res) {
    res.send("Dupa!");
})

app.delete('/workout', function (req, res) {
    Workout.destroy({
        where: {
            id: req.body.id
        }
    }).then(() => {
        res.status(200);
        res.send(workout);
    }).catch(err => {
        res.status(419);
        res.send(err);
    })
})

app.post('/user', function (req, res) {
    User.create(req.body)
        .then(user => {
            res.status(200);
            res.send(user);
        })
        .catch(err => {
            res.status(419);
            res.send(err);
        })
})

app.post('/users', function (req, res) {
    User.create(req.body)
        .then(user => {
            res.status(200);
            res.send(user);
        })
        .catch(err => {
            res.status(419);
            res.send(err);
        })
})

app.delete('/user', function (req, res) {
    User.destroy({
        where: {
            id: req.body.id
        }
    }).then(() => {
        res.status(200);
        res.send(workout);
    }).catch(err => {
        res.status(419);
        res.send(err);
    })
})

app.post('/search', function (req, res) {
    res.send("Dupa!");
})

app.post('/comment', function (req, res) {
    res.send("Dupa!");
})

app.post('/getComments', function (req, res) {
    res.send("Dupa!");
})