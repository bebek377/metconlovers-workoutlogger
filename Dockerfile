FROM node:latest

COPY . /

RUN npm install

EXPOSE 1979

CMD node app.js