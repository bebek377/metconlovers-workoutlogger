var app = angular.module('app', []);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app
    .controller('mainController',
        function ($scope, $http) {

            $scope.apiURL = "http://localhost:1979";
            $scope.spacename = '';
            $scope.pass = '';
            $scope.state = 'none';

            $scope.pageInit = function () {
                console.log("Workout Logger2 init...")
            };

            $scope.checkSpace = function () {
                console.log("T");
                $http({
                    method: 'POST',
                    url: $scope.apiURL + "/checkSpace",
                    data: {
                        space: $scope.spacename
                    }
                }).then(function (result) {
                    $scope.state = 'exist';
                }, function () {
                    $scope.state = 'create';
                });
            };

        });